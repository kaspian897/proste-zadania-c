#include <iostream>
#include <map>
using namespace std;

struct element{
    element * next;
    int id;
};

struct lista {
    element *first;
    element *last;
    void dodaj_element(int id){
        element * nowy=new element;
        nowy->id=id;

        nowy->next=first;
        first=nowy;
    }
};


lista ** listaSasiedztwa;
bool * czyOdwiedzone;

void DFS(int start,int meta){

    element *Stos,*przetwarzany,*pomocniczy;

    przetwarzany = new element;
    przetwarzany->id = start;
    przetwarzany->next = NULL;
    Stos = przetwarzany;

  czyOdwiedzone[start] = true;
  while( Stos )
  {
    start = Stos->id;
    Stos = Stos->next;

    for(przetwarzany=listaSasiedztwa[start]->first; przetwarzany; przetwarzany=przetwarzany->next)
    {

      if(!czyOdwiedzone[przetwarzany->id])
      {
        pomocniczy = new element;
        pomocniczy->id = przetwarzany->id;
        pomocniczy->next = Stos;
        Stos = pomocniczy;
        czyOdwiedzone [ przetwarzany->id ] = true;
        if(przetwarzany->id==meta){
            cout<<"T"<<endl;
            return;
        }
      }
    }

  }
  cout<<"N"<<endl;
}
int main()
{
    map<string, int> slownik;
    int liczbawierzcholkow=0;
    int siz=2;
    listaSasiedztwa=new lista *[siz];
    czyOdwiedzone=new bool[siz];
    for(int i = 0; i < siz; i++ )
    {
        listaSasiedztwa[i] = new lista;
        czyOdwiedzone [i] = false;
    }
    char t;
    string a,b;

    while(cin>>t>>a>>b){
        if(t=='B'){
            if(liczbawierzcholkow+2>=siz){
                lista ** temp;
                temp = new lista * [siz * 2];
                for (int i = 0; i < siz; i++) {

                    temp[i] = listaSasiedztwa[i];
                }
                for(int i = siz; i < siz*2; i++){
                    temp[i] = new lista;
                }
                delete [ ] czyOdwiedzone;


                listaSasiedztwa = temp;
                siz = siz * 2;
                czyOdwiedzone=new bool[siz];

                for(int i = 0; i < siz; i++ )
                {
                    czyOdwiedzone [i] = false;
                }

            }

            int id1=slownik[a];
            int id2=slownik[b];

            if(!id1){
                liczbawierzcholkow++;
                slownik[a]=liczbawierzcholkow;
                id1=liczbawierzcholkow;
            }
            if(!id2){
                liczbawierzcholkow++;
                slownik[b]=liczbawierzcholkow;
                id2=liczbawierzcholkow;
            }

            listaSasiedztwa[id1]->dodaj_element(id2);
            listaSasiedztwa[id2]->dodaj_element(id1);
        }else if(t=='T'){
            /*for(int i=1;i<liczbawierzcholkow;i++){
                    element * el=listaSasiedztwa[i]->first;
                while(el){
                    //cout<<i<<" "<<el->id<<" ";
                    el=el->next;
                }
                //cout<<endl;
            }*/
            int id1=slownik[a];
            int id2=slownik[b];
            DFS(id1,id2);
            for(int i = 0; i < siz; i++ )
            {
                czyOdwiedzone [i] = false;
            }
        }
    }
    return 0;
}
