#include <iostream>
#import <cmath>
using namespace std;

void rot(double & x,double & y,double alpha){
    alpha*=M_PI/180.d;
    double xprim=cos(alpha)*x-sin(alpha)*y;
    double yprim=sin(alpha)*x+cos(alpha)*y;
    x=xprim;
    y=yprim;
}
double len(double x1,double y1,double x2,double y2){
    return sqrt((x1-x2)*(x1-x2)+(y1-y2)*(y1-y2));
}
double lecCircle(double r,double x1,double y1,double x2,double y2){
    double ab=len(x1,y1,x2,y2);
    return asin(r/ab)*r;
}
double circle(double r,double alpha){
    alpha*=M_PI/180;
    return alpha*r;
}
int main()
{
    cout.precision(10);
    double x,y,r;
    double alpha=0;
    double krok1=0.01;
    double lastAlpha=0;
    //cout<<sin(45*M_PI/180);
    /*double *aa=new double[1];
    double *bb=new double[1];
    aa[0]=10;
    bb[0]=0;
    rot(aa[0],bb[0],45);
    cout<<aa[0]<<" "<<bb[0]<<endl;*/
    int t;
    cin>>t;
    for(int i=0;i<t;i++){
        int n;
        cin>>n;
        double * x=new double[n];
        double * y=new double[n];
        double * x2=new double[n];
        double * y2=new double[n];
        int * r=new int[n];
        alpha=0;

        for(int j=0;j<n;j++){
            cin>>x[j]>>y[j]>>r[j];
            x2[j]=x[j];
            y2[j]=y[j];
        }
        //calc max
        double maxi=-10000;
        int ids[2*n];
        for(int j=0;j<2*n;j++){
            ids[j]=-2;
        }
        int index=0;
        int idd=-1;
        double l=0;
        lastAlpha=0;
        double xx;
        while(alpha<=360){
            for(int j=0;j<n;j++){
                rot(x[j],y[j],krok1);
            }
            int id=-1;
            maxi=-10000;
            for(int j=0;j<n;j++){
                if(y[j]+r[j]>maxi){
                    maxi=y[j]+r[j];
                    id=j;
                    xx=x[j];
                }else if(y[j]+r[j]==maxi){
                    if(x[j]>xx){
                        id=j;
                        xx=x[j];
                    }
                }
            }
            if(idd!=id&&index<2*n){
                ids[index]=id;


                if(index>=2){
                    l+=circle(r[index-1],alpha-lastAlpha);
                    cout<<"a: "<<alpha-lastAlpha<<" "<<circle(r[index-1],alpha-lastAlpha)<<endl;
                }
                if(alpha+krok1>=360){
                    //l+=circle(r[index-1],360-lastAlpha);
                   // cout<<"co poszlo\n"<<endl;
                }
                if(index>=1){
                    //l+=len(x[ids[index-1]],y[ids[index-1]]+r[ids[index-1]],x[ids[index]],y[ids[index]]+r[ids[index]]);
                    l+=abs(x[ids[index-1]]-x[ids[index]]);
                    cout<<l<<" l: "<<x[ids[index-1]]-x[ids[index]]<<endl;

                }
                index++;
                idd=id;


                //alpha-=krok1;
                lastAlpha=alpha;
            }

            alpha+=krok1;
        }
        l+=circle(r[index-2],360-lastAlpha);
        //cout<<"a: "<<lastAlpha<<" "<<circle(r[index-1],360-lastAlpha)<<endl;
        cout<<l<<endl;
        //cout<<endl;
        for(int j=0;j<2*n;j++){
            cout<<ids[j]<<endl;
        }

    }
    return 0;
}
