#include <iostream>
#include <map>
using namespace std;
void quick_sort(double *tab,int * ids, int lewy, int prawy)
{
	if(prawy <= lewy) return;

	int i = lewy - 1, j = prawy + 1;
	double pivot = tab[(lewy+prawy)/2];
	while(1)
	{
		while(pivot>tab[++i]);
		while(pivot<tab[--j]);
		if( i <= j){
			swap(tab[i],tab[j]);
			swap(ids[i],ids[j]);
		}
		else
            break;
	}

	if(j > lewy)
        quick_sort(tab,ids, lewy, j);
	if(i < prawy)
        quick_sort(tab,ids, i, prawy);
}
int main()
{
    map<string, int> slownik;
    int n;
    string name;
    string names[300];
    int weight;

    cin>>n;
    int weights[n];

    for(int i=0;i<n;i++){
        cin>>name>>weight;
        slownik[name]=i;
        names[i]=name;
        weights[i]=weight;
    }
    int macierzSasiedztwa[300][300];
    for(int i=0;i<300;i++){
        for(int j=0;j<300;j++)
        {
            macierzSasiedztwa[i][j]=0;

        }
    }
    string name1,name2;
    int m;
    cin>>m;
    for(int i=0;i<m;i++){
        cin>>name1>>name2;
        int id1=slownik[name1];
        int id2=slownik[name2];
        macierzSasiedztwa[id1][id2]=1;
        macierzSasiedztwa[id2][id1]=1;
    }

    int sumaSasiadow[n];
    int sumaSasiadowPrzejecja[n];
    double * cena =new double[n];
    int * ids=new int[n];
    for(int i=0;i<n;i++){
        cena[i]=0;
        ids[i]=i;
    }
    for(int i=0;i<n;i++){
        int suma=0;
        int suma2=1;
        for(int j=0;j<n;j++){
            suma+=macierzSasiedztwa[i][j];
            if(macierzSasiedztwa[i][j]){
                suma2+=weights[j];
            }
        }
        //sumaSasiadow[i]=suma;
        cena[i]=(double)weights[i]/suma2;
        cena[i]*=suma;

        sumaSasiadowPrzejecja[i]=suma2;
    }
    for(int i=0;i<n;i++){
        //cout<<cena[i]<<" "<<ids[i]<<endl;
    }
    //cout<<endl;
    quick_sort(cena,ids,0,n-1);
    for(int i=0;i<n;i++){
        //cout<<cena[i]<<" "<<ids[i]<<endl;
    }
    //cout<<endl;

    int waga=0;
    int used=0;

    int tab1[n];
    int tab2[n];

    for(int i=0;i<n;i++){
        tab1[i]=0;
        tab2[i]=0;
    }

    int id=0;
    while(id<n){
        int prz=ids[id];
        tab2[prz]++;
        if(!tab1[prz]){
            used++;
        }
        waga+=weights[prz];
        for(int i=0;i<n;i++){
            if(macierzSasiedztwa[prz][i]){
                if(!tab1[i]&&!tab2[i]){
                    used++;
                }
                tab1[i]++;
            }
        }

        id++;

        if(used>=n){
            break;
        }
    }
    int j=id-1;
    while(j>=0){
        int prz=ids[j];
        tab2[prz]--;
        waga-=weights[prz];
        id--;
        for(int i=0;i<n;i++){
            if(macierzSasiedztwa[prz][i]){
                tab1[i]--;
            }
        }
        bool f=true;
        for(int i=0;i<n;i++){
            if(tab1[i]<=0&&tab2[i]<=0){
                f=false;
                break;
            }
        }
        if(!f){
            tab2[prz]++;
            id++;
            waga+=weights[prz];
            for(int i=0;i<n;i++){
                if(macierzSasiedztwa[prz][i]){
                    tab1[i]++;
                }
            }
        }

        j--;
    }

    cout<<id<<endl;
    for(int i=0;i<n;i++){
        if(tab2[i]){
            cout<<names[i]<<endl;
        }
    }
    cout<<waga<<endl;

    return 0;
}
