#include <iostream>

using namespace std;
long long pow(long long liczba, unsigned int potega)
{
	long long wynik = 1;
	while(potega>0)
	{
		if (potega%2 == 1){
			wynik *= liczba;
		}
		liczba*= liczba;
		potega/=2;
	}
	return wynik;
}
int main()
{
    int r=0,s=0;
    cin>>r>>s;
    int i=0,j=0;

    if(r==0){
        cout<<0;
    }
    else if(((int)s/r)>4)
    {
        cout<<pow(10,r);
    }else{
        i=s/r;
        j=s%r;
        cout<<pow((i+1)*2,r-j)*pow((i+2)*2,j);
    }
    return 0;
}
