#include <iostream>
#include <map>
using namespace std;


int * wierzcholki;
int * ran;
int checkRoot(int i)
{
    while (wierzcholki[i] != i)
    {
       wierzcholki[i] = wierzcholki[wierzcholki[i]];
       i = wierzcholki[i];
    }
    return i;
}
void link(int a, int b)
{
    int rootA = checkRoot ( a);
    int rootB = checkRoot ( b);

    if (ran[rootA] < ran[rootB])
    {
       wierzcholki[rootA] = wierzcholki[rootB];
       ran[rootB] += ran[rootA];
    }
    else
    {
        wierzcholki[rootB] = wierzcholki[rootA];
        ran[rootA] += ran[rootB];
    }
}
bool compareRoot( int a, int b)
{
    return (checkRoot( a) == checkRoot(b));
}
void checkCnnectivity(int x, int y)
{

        if (compareRoot( x, y)){
            cout << "T" << endl;
        }
        else{
           cout << "N" << endl;
        }
}
void add(int x, int y){
    if (!compareRoot( x, y) ){
        link( x, y);
    }
}

int main()
{
    map<string, int> slownik;
    int size = 2048;
    int liczbawierzcholkow=0;
    wierzcholki=new int[size];
    ran=new int[size];
    for (int i=0; i<size; i++)
    {
        wierzcholki[i] = i;
        ran[i] = 1;
    }




    char t;
    string a,b;

    while(cin>>t>>a>>b){
            if(liczbawierzcholkow+2>=size){
                int * temp= new int[size * 2];
                int *tempRank=new int[size*2];
                for (int i = 0; i < size; i++) {
                    temp[i] = wierzcholki[i];
                    tempRank[i]=ran[i];
                }
                for(int i = size; i < size*2; i++){
                    temp[i] = i;
                    tempRank[i]=1;
                }
                delete [ ] ran;
                delete [ ] wierzcholki;


                wierzcholki = temp;
                ran=tempRank;
                size = size * 2;

            }
        if(t=='B'){


            int id1=slownik[a];
            int id2=slownik[b];

            if(!id1){
                liczbawierzcholkow++;
                slownik[a]=liczbawierzcholkow;
                id1=liczbawierzcholkow;
            }
            if(!id2){
                liczbawierzcholkow++;
                slownik[b]=liczbawierzcholkow;
                id2=liczbawierzcholkow;
            }
            add(id1,id2);

        }else if(t=='T'){
            int id1=slownik[a];
            int id2=slownik[b];
            if(!id1){
                liczbawierzcholkow++;
                slownik[a]=liczbawierzcholkow;
                id1=liczbawierzcholkow;
            }
            if(!id2){
                liczbawierzcholkow++;
                slownik[b]=liczbawierzcholkow;
                id2=liczbawierzcholkow;
            }
            checkCnnectivity(id1,id2);

        }
    }
    return 0;
}
